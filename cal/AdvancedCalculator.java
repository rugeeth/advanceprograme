package cal;

public class AdvancedCalculator extends BasicCalculator {

	double findSin(double degree){
		double result = Math.sin(Math.toRadians(degree));
		return result;
	}
	
	double findCos(double degree){
		double result = Math.cos(Math.toRadians(degree));
		return result;
	}
	double findTan(double degree){
		double result = Math.tan(Math.toRadians(degree));
		return result;
	}
	
	
}


