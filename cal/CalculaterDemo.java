package cal;

public class CalculaterDemo {

	public static void main(String[] args) {
		
		Child child = new Child();
		
		System.out.println(child.addNumber(5, 8));
		System.out.println(child.subNumber(10, 5));
		System.out.println(child.divNumber(25, 5));
		System.out.println(child.maltyNumber(10, 5));
		
	}
}
