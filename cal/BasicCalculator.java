package cal;

public class BasicCalculator {

	public int addNumber(int x, int y) {
		int result = x + y;
		return result;
	}
	public int subNumber(int x, int y) {
		int result = x - y;
		return result;
	}
	public int maltyNumber(int x, int y) {
		int result = x * y;
		return result;
	}
	public int divNumber(int x, int y) {
		int result = x / y;
		return result;
	}
	public int modNumber(int x, int y) {
		int result = x % y;
		return result;
	}
	
}
