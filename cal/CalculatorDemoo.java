package cal;

public class CalculatorDemoo {

	public static void main(String[] args) {
	
		AdvancedCalculator cal = new AdvancedCalculator();
		
		System.out.println(cal.addNumber(10, 5));
		System.out.println(cal.divNumber(25, 5));
		System.out.println(cal.maltyNumber(10, 5));
		System.out.println(cal.subNumber(10, 5));
		System.out.println(cal.modNumber(50, 5));
		
		System.out.println(cal.findSin(30));
		System.out.println(cal.findCos(90));
		System.out.println(cal.findTan(45));
		
		}
}
