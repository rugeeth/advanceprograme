package exception;

public class Account {

	private double balance;
	private String accNumber;
	
	public Account(String accNumber) {
		this.accNumber = accNumber;
	}
	
	public void deposit(double amount) throws MinDepositException, MaxDepositException{
		if(amount < 100) {
			throw new MinDepositException();
		}else if (amount > 100000) {
			throw new MaxDepositException();
		}else {
			balance += amount;
		}
	}
	
	public void withdrowal(double amount) throws MinWithdrowalException , MaxWithdrowalException{
		if(amount < 100) {
			throw new MinWithdrowalException();
		}else if (amount > 200000){
			throw new MaxWithdrowalException();
		}else {
			balance -= amount;
		}
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getAccNumber() {
		return accNumber;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}
	
	
}
