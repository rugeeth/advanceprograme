package exception;

public class MinDepositException extends Exception {

	public MinDepositException() {
		System.out.println("Minimum deposit amount is 100");
	}
}
