package ap.db.HW;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;



public class JDBCSingleton {

	private static JDBCSingleton jdbc = null;
	
	private JDBCSingleton() {
		
	}
	
	public static JDBCSingleton getInstance() {
		if(jdbc == null) {
			jdbc = new JDBCSingleton();
		}
		return jdbc;
	}
	
	public static Connection getConnection() throws SQLException {
		
		Connection connection = null;
		connection = DriverManager.getConnection(DBProperties.URL.value +  DBProperties.DATABASE.value, DBProperties.USERNAME.value, DBProperties.PASSWORD.value);
		return connection;
	}
	
	public void insert(String Name, String Address, int PhoneNo) throws ClassNotFoundException, SQLException {
		
		Connection connection = null;
		PreparedStatement statement = null;
		
		
		connection = getConnection();
		statement = connection.prepareStatement("INSERT INTO student_details(Name,Address,PhoneNo)VALUES(?,?,?)");
		statement.setString(1, Name);
		statement.setString(2, Address);
		statement.setInt(3, PhoneNo);
		int rowCount = statement.executeUpdate();
		
		if(statement != null) {
			statement.close();
		}
		if(connection != null) {
			connection.close();
		}
	}
		
	public void view (String Name) throws ClassNotFoundException, SQLException {
			
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			
			connection = getConnection();
			statement = connection.prepareStatement("SELECT * FROM student_details WHERE Name = ?" );
			statement.setString(1, Name);
			resultSet = statement.executeQuery();
			
			while(resultSet.next()) {
				System.out.println("Hello " + resultSet.getString(1) + "\n" + "Your Address = " + resultSet.getString(2) + "\n" + "Your phone Number :" + resultSet.getInt(3));
			}
			
			if(statement != null) {
				statement.close();
			}
			if(connection != null) {
				connection.close();
		}
	}
		
	public void delete(String Name) throws ClassNotFoundException, SQLException {
			
			Connection connection = null;
			
			connection = getConnection();
			int rowCount = connection.prepareStatement("DELETE FROM student_details WHERE Name = '"+Name+"'").executeUpdate();
			

			if(connection != null) {
				connection.close();
			}
			System.out.println("Deleted Data Row count : " + rowCount);
	}
		
	public void update(String Name, String address, int PhoneNo) throws ClassNotFoundException, SQLException {
		
			Connection connection = null;
			
			connection = getConnection();
			int count = connection.prepareStatement("UPDATE student_details SET Address = '"+address+"',PhoneNo = '"+PhoneNo+"' WHERE Name = '"+Name+"'").executeUpdate();
			
			
			if(connection != null) {
				connection.close();
			}		
		}	
	
	}