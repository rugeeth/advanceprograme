package ap.db.HW;

import java.sql.SQLException;
import java.util.Scanner;

public class JDBCSingletonDemo {
	
	public static int n;
	
	public static void loopp() {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("----------WELCOME---------");
		System.out.println("If you Insert Data press ---> 1.");
		System.out.println("If you View the Data press ---> 2.");
		System.out.println("If you Delete Data press ---> 3.");
		System.out.println("If you Update Data press ---> 4.");
		System.out.println("If you Exit  press ---> 5.");
		n = sc.nextInt();
		System.out.println("");
	}
	
	public static void main(String[] args) {
		
		JDBCSingleton db = JDBCSingleton.getInstance();
		 Scanner scan = new Scanner(System.in);
		
		 loopp();
		switch (n) {
		
		case 1 : try {
					System.out.println("Enter your Name");
					String name = scan.next();
					System.out.println("Enter your Address");
					String Address = scan.next();
					System.out.println("Enter your Phone Number");
					int number = scan.nextInt();
					db.insert(name,Address,number);
					System.out.println("Data successfully Insert");
					System.out.println("");
					
						} catch (ClassNotFoundException | SQLException e) {
							e.printStackTrace();
						}
					loopp();
					
					
		case 2 : try {
					System.out.println("Enter your Name");
					String name = scan.next();
					db.view(name);
					System.out.println("");
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					}
					loopp();
					
					
		case 3 :try {
					System.out.println("Enter your Name");
					String name = scan.next();
					db.delete(name);
					System.out.println("Data successfully Deleted");
					System.out.println("");
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					}
					loopp();
					
					
		case 4 :try {
					System.out.println("Enter your Name");
					String name = scan.next();
					System.out.println("Enter Your New Address");
					String address = scan.next();
					System.out.println("Enter Your New Phone Number");
					int number = scan.nextInt();
					db.update(name,address,number);
					System.out.println("Data successfully Update");
					System.out.println("");
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					}
					loopp();
				
					
		case 5 : {
					System.out.println("System Exit");	
					}
				break;
		}			
	}	
}
